// redeploy v0.1

import axios from 'axios';
import _get from "lodash/get";

export default {
  bridge: true,
  devServerHandlers: [],
  serverHandlers: [],

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Baltu Rotas',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  publicRuntimeConfig: {
    TRUE_HOST: process.env.TRUE_HOST || 'https://balturotas.lv/',
    DISABLE_STATICALLY: process.env.DISABLE_STATICALLY || false,
    // googleAnalytics: {
    //   id: process.env.GOOGLE_ANALYTICS_ID
    // }
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/scss/main.scss',
    'vue-slick-carousel/dist/vue-slick-carousel.css'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    'nuxt-ssr-cache',

    '@nuxtjs/sentry',

    // https://go.nuxtjs.dev/buefy
    ['nuxt-buefy', { css: false }],
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/i18n',
    '@nuxtjs/axios',
    '@nuxtjs/gtm',
    '@nuxtjs/dayjs',
    '@nuxtjs/robots',
    '@nuxtjs/sitemap',
    // 'cookie-universal-nuxt',
  ],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    // '@nuxtjs/eslint-module',
    '@nuxtjs/google-fonts',
    // '@nuxtjs/google-analytics',
    'nuxt-delay-hydration',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: {
    dirs: [
      '~/components',
      '~/components/pages/basket',
      '~/components/pages/checkout',
    ],
  },

  router: {
    middleware: [
      // 'maintenance',
      'i18n',
    ],
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    // { src: '~/plugins/i18n.js' },
    { src: '~/plugins/axios.js' },

    '~/plugins/utils.js',
    '~/plugins/vue-slick-carousel.js',
    '~/plugins/directives.js',
    '~/plugins/imagekit.js',
    '~/plugins/vee-validate.js',

    { src: '~/plugins/infiniteloading', ssr: false },
    { src: '~/plugins/vue-plyr.js', ssr: false },
    { src: '~/plugins/aos.js', ssr: false },
  ],

  // Google Fonts module configuration: https://google-fonts.nuxtjs.org/options
  googleFonts: {
    families: {
      Manrope: true
    },
  },

  // Day.js module configuration
  dayjs: {
    // locales: ['en', 'ja'],
    // defaultLocale: 'en',
    // defaultTimeZone: 'Asia/Tokyo',
    plugins: [
      // 'utc', // import 'dayjs/plugin/utc'
      // 'timezone' // import 'dayjs/plugin/timezone'
      'customParseFormat' // import 'dayjs/plugin/customParseFormat'
    ]
  },


  sentry: {
    dsn: process.env.SENTRY_DSN || 'https://cbdc3577a1cd4bf19d442312181b836f@o4504650728275968.ingest.sentry.io/4505521902977024', // Enter your project's DSN.

    tracing: true,
    tracesSampleRate: 0.2,
    browserTracing: {},
    vueOptions: {
      trackComponents: true,
    },
  },

  robots: {
    configPath: './robots.config',
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // baseURL: process.env.BACKEND_URL || 'https://cms.balturotas.lv',
    credentials: true,
    withCredentials: true,

    proxy: true,

    // debug: true,
    proxyHeaders: false,
    // headers: {
    //   'Access-Control-Allow-Credentials': true,
    //   'Access-Control-Allow-Origin': '*',
    //   'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS'
    // }
  },

  proxy: {
    '/api/': {
      target: process.env.BACKEND_URL || 'https://cms.balturotas.lv',
      // xfwd: true,
      // followRedirects: true,
    },
    '/storage/': process.env.BACKEND_URL || 'https://cms.balturotas.lv',
  },

  // i18n module configuration: https://i18n.nuxtjs.org/options-reference
  i18n: {
    defaultLocale: 'en',
    strategy: 'prefix',
    locales: [
      { code: 'en', file: 'get.js', name: 'English', iso: 'en-GB', flag: 'gb' },
      { code: 'lv', file: 'get.js', name: 'Latviešu', iso: 'lv-LV', flag: 'lv' },
      { code: 'de', file: 'get.js', name: 'Deutsch', iso: 'DE-DE', flag: 'de' }
      // @by Guntis
      // This is an Example how we will use language strings. See file ~/lang/get.js
      // { code: 'ru', file: 'get.js', name:'RU', iso: 'ru-RU' },
    ],
    lazy: true,
    langDir: 'lang/',
  },

  // Server config: https://nuxtjs.org/docs/configuration-glossary/configuration-server
  server: {
    host: process.env.SERVER_HOST || '0.0.0.0', // default: localhost... use 0, to assign computer IP and open app in local network
    port: process.env.SERVER_PORT || '3000',
  },

  gtm: {
    id: 'GTM-TGT9WPX',
    enabled: true,
  },

  sitemap: {
    hostname: process.env.TRUE_HOST || 'https://balturotas.lv',
    i18n: false,

    defaults: {
      changefreq: 'daily',
      priority: 1,
      lastmod: new Date()
    },

    routes: async () => {
      const toRet = [];
      const axiosBaseUrl = process.env.BACKEND_URL || 'https://cms.balturotas.lv';


      const [
        products,
      ] = await Promise.all([
        axios.get(`${axiosBaseUrl}/api/v1/products/all`),
      ]);

      for (let i = 0; i < products.data.data.length; i++) {
        const item = products.data.data[i];
        let slug = item['slug'];
        let variation_id = item['variation_id'];

        toRet.push(stringInject('product/{slug}/{variation_id}', {
            slug: slug,
            variation_id: variation_id
          })
        );
      };
      return toRet;
    }
  },

  delayHydration: {
    mode: 'manual',
    debug: process.env.NODE_ENV === 'development',

    exclude: [
      '*/checkout'
    ],
  },

  version: 0.1,
  cache: {

    // if you're serving multiple host names (with differing
    // results) from the same server, set this option to true.
    // (cache keys will be prefixed by your host name)
    // if your server is behind a reverse-proxy, please use
    // express or whatever else that uses 'X-Forwarded-Host'
    // header field to provide req.hostname (actual host name)
    useHostPrefix: false,
    pages: [
      '/',
      // these are prefixes of pages that need to be cached
      // if you want to cache all pages, just include '/'

      // to cache only root route, use a regular expression
      // /^\/$/
    ],

    store: {
      type: 'memory',

      // maximum number of pages to store in memory
      // if limit is reached, least recently used page
      // is removed.
      max: 10,

      // number of seconds to store this page in cache
      ttl: 60,
    },
  },

  // generate: {
  //   // Interval in milliseconds between two render cycles to avoid
  //   // flooding a potential API with calls from the web application.
  //   interval: 100,
  // },

  // googleAnalytics: {
  //   id: process.env.GOOGLE_ANALYTICS_ID
  // },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: [
      "vue-intersect",
      "vee-validate/dist/rules"
    ]
  }
}
