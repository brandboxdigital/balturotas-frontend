export default function ({redirect,route,i18n, localePath}) {
  const isMaintenance = true;

  if(isMaintenance){
    return redirect(localePath('/maintenance'));
  }
 }
