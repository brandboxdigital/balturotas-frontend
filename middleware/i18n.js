import { createLocaleFromRouteGetter } from '@nuxtjs/i18n/src/templates/utils-common';
import { DEFAULT_OPTIONS } from '@nuxtjs/i18n/src/helpers/constants';

export default async function ({$axios, redirect, route, i18n, localePath, req}) {

  const getLocaleFromRoute = createLocaleFromRouteGetter(i18n.localeCodes, {
    routesNameSeparator: DEFAULT_OPTIONS.routesNameSeparator,
    defaultLocaleRouteNameSuffix: DEFAULT_OPTIONS.defaultLocaleRouteNameSuffix
  })

  try {

    if (getLocaleFromRoute(route) == '') {

      const requestIp = require('request-ip');
      const xRealIp   = requestIp.getClientIp(req)

      const langResponse = await $axios.$get(`/api/v1/location/country-code-from-ip/${xRealIp}`)

      if (langResponse && langResponse.data) {
        let country = langResponse.data;

        return redirect( localePath(route,country) );
      }
    }
  } catch (error) {

  }

 }
