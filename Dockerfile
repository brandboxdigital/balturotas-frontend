# Build
FROM node:20-alpine as builder
WORKDIR /app
RUN apk --no-cache add openssh g++ make python3 git
COPY package.json /app/
RUN yarn install && yarn cache clean --force
ADD . /app
RUN yarn build

# Run
# FROM node:14-alpine
# FROM --platform=linux/arm64 arm64v8/node:14-alpine
FROM node:20-alpine
WORKDIR /app
COPY --from=builder /app /app
# COPY . /app
EXPOSE 3000
# ENTRYPOINT ["node", ".nuxt/server/index.mjs"]
CMD [ "yarn", "start" ]
