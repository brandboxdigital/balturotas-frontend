/**
 * Get all strings from backend
 */
export default async ({ $axios }, locale) => {
  $axios.setHeader('Accept-Language', locale)

  let strings = {};

  try {
    strings = await $axios.$get('/api/v1/content/static-strings')
  } catch (error) {}

  return {
    // '__': getStaticStrings(locale),
    ...strings.data,
  }
}

function getStaticStrings(locale) {

  const statisstrings = {
    lv: {
      maintenance: {
        title: 'Lūdzu atgriezies lapā vēlāk.',
        body: 'Lapā šobrīd tiek veikti tehniski uzlabojumi. Pēc brīža lūdzu atsvaidzini lapu.',
        button: 'Atsvaidzināt',
      }
    },
    en: {
      maintenance: {
        title: 'Please return to the page later.',
        body: 'Technical improvements are currently being made to the page. Please refresh the page after a while.',
        button: 'Refresh',
      }
    },
    de: {
      maintenance: {
        title: 'Bitte kehren Sie später zur Seite zurück.',
        body: 'Die Seite wird derzeit technisch verbessert. Bitte aktualisieren Sie die Seite nach einer Weile.',
        button: 'Aktualisieren',
      }
    },
  }

  if (statisstrings[locale]) {
    return statisstrings[locale];
  }

  return {};
}
