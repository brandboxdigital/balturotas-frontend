import cloneDeep from 'lodash/cloneDeep'

function getRandomIntInclusive (min, max) {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min + 1)) + min // Max and min included
}

const defaultState = {
  list: [],
  options: {
    jewelryType: {
      title: 'Rotas Veids',
      type: 'checkboxes',
      children: [
        // { title: 'Aproču pogas', key: 'cufflinks' }
      ]
    },
    colorKey: {
      title: 'Krāsojums',
      type: 'checkboxes2',
      children: []
    },
    size: {
      title: 'Aproču izmēri',
      type: 'sizes',
      note: 'Izmēra mērvienība norādīta Ø mm',
      children: Array.from(Array(30)).map((_, i) => i + 18)
    }
  },
  filters: {
    jewelryType: {},
    colorKey: {},
    size: {}
  }
}

export const state = () => defaultState

export const mutations = {
  SET_LIST (state, payload) {
    state.list = payload
  },
  SET_FILTERS (state, payload) {
    state.filters = cloneDeep(payload)
  }
}

export const getters = {
  // FIXME only works with TRUE/FALSE
  filteredProducts: state => state.list.filter((product) => {
    return Object.entries(state.filters).every(([property, selected]) => {
      if (Object.keys(selected).filter(key => selected[key]).length) {
        return Object.keys(selected).filter(key => selected[key]).some((key) => {
          if (typeof selected[key] === 'boolean') {
            if (property == 'jewelryType') {
              return String(product.type_id) === key
            }
            if (property == 'size') {
              return String(product[property]) === key
            }
            if (property == 'colorKey') {
               return String(product.color.id) === key
            }
          }
          return false
        })
      } else {
        return true
      }
    })
  }),
  isSizeAvailable: state => size => state.list.map(product => parseInt(product.size)).includes(size)
}

export const actions = {
  async fetchProducts ({ commit }) {

    // let response = await this.$axios.$get('/api/v1/products/all?page=1&perPage=100')
    // let jevelry = await this.$axios.$get('/api/v1/products/types')
    // let colors = await this.$axios.$get('/api/v1/products/colors')

    // defaultState.options.jewelryType.children = jevelry.data
    // defaultState.options.colorKey.children = colors.data

    return new Promise((resolve, reject) => {
      // TODO integrate backend
      // this.$axios.get('/products').then((r) => {
      //   commit('SET_LIST', r.data)
      //   resolve(r.data)
      // }).catch((e) => {
      //   reject(e)
      // })
      // let id = 0
      // const products = []
      // Array.from(Array(30)).forEach(() => {
      //   const randomColor = defaultState.options.colorKey.children[getRandomIntInclusive(0, 7)]
      //   products.push({
      //     id: ++id,
      //     code: `SS–${getRandomIntInclusive(1, 1000)}`,
      //     main_image: {
      //       path: `/demo/products/${getRandomIntInclusive(1, 8)}.png`
      //     },
      //     name: 'Product Name Example Here',
      //     price: `${getRandomIntInclusive(0, 99)}.${getRandomIntInclusive(0, 99)}`,
      //     color: randomColor.background,
      //     colorName: randomColor.title,
      //     colorKey: randomColor.key,
      //     size: defaultState.options.size.children[getRandomIntInclusive(0, 20)],
      //     jewelryType: defaultState.options.jewelryType.children[getRandomIntInclusive(0, 5)].key
      //   })
      // })
      // commit('SET_LIST', products)
      // resolve(products)

      // commit('SET_LIST', response.data.data)
      resolve()
    })
  }
}
