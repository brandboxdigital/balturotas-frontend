
import VueI18n from 'vue-i18n';

import { extend, localize } from "vee-validate";
import { required, alpha, digits, email, max } from "vee-validate/dist/rules";

import lv from 'vee-validate/dist/locale/lv.json';
import en from 'vee-validate/dist/locale/en.json';
import de from 'vee-validate/dist/locale/de.json';

const customMessages = {
    lv: {
      phone:             'Kļūdains telefona numurs. Numurs sastāv no skaitļiem.',
      'gift-card-value': 'Lauka vērtība nevar būt 0',
    },

    en: {
      phone:             'Wrong phone number. The number consists of numbers.',
      'gift-card-value': 'Field value cannot be 0',
    },

    de: {
      phone:             'Falsche Telefonnummer. Die Zahl besteht aus Zahlen.',
      'gift-card-value': 'Feldwert darf nicht 0 sein.',
    },
}

extend('email', email);
extend('required', required);
extend('max', max);

// extend("required", {
//   ...required,
//   message: "ir obligāts"
// });

// extend('required', {
//   ...required,
//   // the values param is the placeholders values
//   message: (_, values) => i18n.$t('validations.messages.required', values)
// });

extend("alpha", {
  ...alpha,
  message: "This field must only contain alphabetic characters"
});

extend('phone', {
  validate: value => {
    return /^\+?[0-9]*\s?[0-9]*$/.test(value) && value.length > 7;
  },
});

extend('agree_to_rules', {
  validate: value => {
    return value === true;
  },
  message: "formvalidate.agree_to_rules",
});

extend('gift-card-value', {
  validate: value => {
    //console.log('a?', value);
    return value > 0;
  },

  message: "Vērtība nevar būt 0",
});

export default ({ app: { i18n } }) => {

  const dictionaries = prepareDictionaries();

  if (typeof dictionaries[i18n.locale] !== 'undefined') {
    localize(i18n.locale, dictionaries[i18n.locale]);
  } else {
    localize('lv', dictionaries['lv']);
  }
};


function prepareDictionaries()
{
  //Spread the imports and overrides, to make static object!

  const dictionaries = {
    lv: {
      code: lv.code,
      messages: {
        ...lv.messages,
        ...customMessages['lv'],
      }
    },
    en: {
      code: en.code,
      messages: {
        ...en.messages,
        ...customMessages['en'],
      }
    },
    de: {
      code: de.code,
      messages: {
        ...de.messages,
        ...customMessages['de'],
      }
    },
  }

  return dictionaries;
}
