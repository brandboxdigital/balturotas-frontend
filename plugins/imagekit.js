const crypto = require('crypto');

function sign(path, secret) {
  const hash = crypto.createHmac('sha1', secret)
          .update(path)
          .digest('base64')
          .replace(/\+/g, '-').replace(/\//g, '_')
  return hash + '/' + path
}


export default (context, inject) => {

  function fImageKit(url, width, height)  {

    //console.log("fImageKit", url);

    if (!url) {
      return '';
    }

    if (typeof url === 'object') {

      if (Object.keys(url).indexOf('path') > -1) {
        url = url.path;
      } else {
        return '';
      }
    }

    // Check if we need to run Nuxt in development mode
    // const isDev = process.env.NODE_ENV !== 'production'
    const isDev   = false
    const disable = context?.$config?.DISABLE_STATICALLY || false;

    if (isDev || disable) {
      return url;
    }

    /**
     * Already is stacally link..
     */
    if (url.indexOf('imagor.brandbox.digital') !== -1 || url.indexOf('image.balturotas.lv') !== -1) {
      return url;
    }

    let link   = url;
    let domain = 'https://balturotas.lv';

    try {
      //  domain = context?.$config?.TRUE_HOST;
    } catch (error) {}

    if (url.indexOf('http://') === -1 && url.indexOf('https://') === -1) {
    } else {
      const noHttp = url.replace('http://','').replace('https://','');

      domain = noHttp.split(/[/?#]/)[0];
      link   = noHttp.replace(domain+"/", '');
    }

    try {
      domain = domain.replace('http://','').replace('https://','').replace(/\/$/, '');
    } catch (error) {

    }


    // https://cms.balturotas.brandbox.digital/storage/app/uploads/public/61c/06c/5e7/61c06c5e760bf854907992.jpg
    // https://ik.imagekit.io/bboxdigi/balturotas/storage/app/uploads/public/61c/06c/5e7/61c06c5e760bf854907992.jpg?tr=2-300,h-300

    // https://imagor.brandbox.digital/unsafe/fit-in/200x200/cms.balturotas.lv/storage/app/uploads/public/639/99f/8e6/63999f8e61e9d553932856.jpg

    let imageUrl = `${domain}/${link}`;

    if (width && height) {
      imageUrl = `fit-in/${width}x${height}/${domain}/${link}`;
    } else if (width) {
      imageUrl = `fit-in/${width}x${width}/${domain}/${link}`;
    }

    try {
      imageUrl = imageUrl.replace('//','/')
    } catch (error) {}

    return `https://image.balturotas.lv/` + sign(imageUrl, 'bboxdigi');

  }

  inject('imagekit', fImageKit)
}
