export default function ({ $axios, $auth, redirect, app }) {

  $axios.onRequest(config => {
    config.headers.common['Accept'] = 'application/json'
    config.headers.common['Content-Type'] = 'application/json'
    config.headers.common['Accept-Language'] = app.i18n.locale
  });

  $axios.onError(error => {
    console.error("Axios",error)
  })

  $axios.onResponseError(error => {
    console.error("Axios",error);
  })

  // $axios.onResponse(response => {
  //   console.error("response",response)
  //   console.error("response",$cookies.getAll({ fromRes: true }))
  //   $axios.defaults.headers.common.cookie
  //   axios.defaults.headers.common.cookie = req.headers.cookie
  // })


}
