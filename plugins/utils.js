import Vue from 'vue'
import slugify from 'slugify'
import _get from "lodash/get";

const parseCurrency = (price, count = 1, decimals = 2, currency = 'EUR') => {

  let _price = price || 0;
  let _count = count || 1;
  let _decimals = decimals || 2;
  let _currency = currency || null;

  // If price is money object
  if (typeof price === "object") {
    _price = price.amount;
    _currency = price.currency;
  }

  const amount = _price * _count / 100;

  if (_currency === null) {
    return amount / 100;
  }

  return Number(amount || 0).toLocaleString([], {
    style: 'currency',
    currency: _currency,
    minimumFractionDigits: _decimals
  });
}

const scrollTo = (id, yOffset = 0) => {
  if (process.client) {

    const element = document.getElementById(id)

    if (element) {
      const y = element.getBoundingClientRect().top + window.pageYOffset + yOffset

      window.scrollTo({
        top: y,
        behavior: 'smooth',
      })
    } else {
      window.scrollTo({
        top: 0 + yOffset,
        behavior: 'smooth',
      })
      // alert(`Element with ID ${id} not found`)
    }
  } else {

  }
}

const optional = function (obj, param, def = null) {
  return _get(obj, param, def);
}

const mediaLibraryUrl = function (path) {
  let backend_url = process.env.BACKEND_URL || null;

  try {
    backend_url = $nuxt.$config.BACKEND_URL;
  } catch (error) { }

  if (path.indexOf('http://') === -1 && path.indexOf('https://') === -1) {
    return `${backend_url}/storage/app/media/${path}`;
  }

  return path;
}

const sanitizeUrl = function (url) {
  if (url.indexOf('http://') === -1 && url.indexOf('https://') === -1) {
    return `https://${url}`;
  }

  return url;
}

export default (_, inject) => {
  const utils = {
    parseCurrency,
    scrollTo,
    slugify,
    optional,
    mediaLibraryUrl,

  }
  // Helper for WebStorm
  Vue.prototype.$utils = utils
  inject('utils', utils)
}
